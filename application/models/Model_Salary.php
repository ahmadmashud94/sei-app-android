<?php
class model_salary extends ci_model{

	public function __construct() 
	{
		parent::__construct(); 
		$this->load->database();
	}
	public function get_all($period = null){
		if ($period == null) {
			$query= "SELECT tg.*,tk.nama FROM tbl_gaji tg inner join tbl_karyawan tk on tg.nik = tk.nik where tg.bulan  = MONTH(now()) and tg.tahun = YEAR(now())";
		}else{
			$bulan = getMonthOfPeriod($period);
			$tahun = getYearOfPeriod($period);
			$query= "SELECT tg.*,tk.nama FROM tbl_gaji tg inner join tbl_karyawan tk on tg.nik = tk.nik where tg.bulan  = $bulan and tg.tahun = $tahun ";
		}
		return $this->db->query($query)->result();
	}
	function save($data)
	{
		$this->db->insert_on_duplicate_update_batch("tbl_gaji", $data);
		return true;
	}

	function save_final($data)
	{
		$this->db->update_batch("tbl_gaji", $data,"nik");
		return true;
	}
	public function get_data_by_nik_and_period($nik,$period){
		$bulan = getMonthOfPeriod($period);
		$tahun = getYearOfPeriod($period);
		$query= "select
		tg.bulan,
		tg.tahun,
		tk.saldo_tabungan,
		tg.pinjaman_koperasi,
		tg.angsuran_pinjaman,
		tk.sisa_cuti,
		tk.nama,
		tk.status,
		tk.gaji_pokok,
		tg.tunjangan_karyawan,
		tg.tunjangan_kehadiran_transport,
		tg.tunjangan_jabatan,
		tg.tunjangan_masa_kerja,
		tg.tunjangan_shift,
		tg.insentif_kehadiran,
		tg.pph21_dtp,
		tg.overtime_jam,
		tg.overtime,
		tg.total,
		tg.potongan_absensi,
		tg.simpanan_koperasi,
		tg.iuran_jht,
		tg.iuran_pensiun,
		tg.bpjs_kesehatan,
		tg.pph21
		from
		tbl_gaji tg
		inner join tbl_karyawan tk on
		tg.nik = tk.nik
		where
		tg.bulan = $bulan
		and tg.tahun = $tahun
		and tg.nik = '$nik'; ";

		return $this->db->query($query)->row();
	}

}