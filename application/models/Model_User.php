<?php
class model_user extends ci_model{

	public function __construct() 
	{
		parent::__construct(); 
		$this->load->database();
	}
	public function get_all(){
		$query= "SELECT * FROM tbl_user";
		return $this->db->query($query)->result();
	}
	function save($data)
	{
		$this->db->insert_on_duplicate_update_batch('tbl_user', $data);
		return true;
	}
}