<style type="text/css">

table{
	font-size: 10px;
	text-align: center;
}
table td{
	width: 100px;
	white-space: nowrap;
}
*{
	text-align: center;
}
</style>


<div class="app-main__inner">
	<div class="app-page-title">
		<div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon">
					<i class="pe-7s-user icon-gradient bg-happy-itmeo">
					</i>
				</div>
				<div>
					<h4 >
					INFORMASI GAJI <br><?php echo $_SESSION['user_session_android'][0]->nama; ?> (<?php echo $_SESSION['user_session_android'][0]->username; ?>)</h4>
				</div>
			</div>  
		</div>  
	</div>
	<div class="row ">
		<div class="col-lg-12">
			<div class="main-card col-lg-6 card">
				<div class="card-body">
					<form method="GET" action="<?php echo base_url(); ?>Controller_Salary/print_slip_gaji">
						<div class="row">

							<div class="col-md-12">
									<table align="center" border="1" style="text-align:center;">
										<tr>
											<td colspan="2"><b>Koperasi</b></td>
											<td colspan="2"><b>Cuti Karyawan</b></td>
										</tr>
										<tr>
											<td>Sisa Saldo : </td>
											<td>Rp <?php echo @toFormatMoney($data->saldo_tabungan); ?></td>
											<td rowspan="2">Sisa Cuti : </td>
											<td rowspan="2"><?php echo @$data->sisa_cuti; ?></td>
										</tr>
										<tr>
											<td >Sisa Pinjaman : </td>
											<td>Rp <?php echo @toFormatMoney($data->sisa_pinjaman); ?></td>
										</tr>
									</table>
								</div>
								<br>
								<br>
								<br>
							<div class="col-md-12">
								<div class="form-group">
									<label for="showEasing">Pilih Bulan Gaji</label>
									<input required="" id="showEasing" type="month" class="form-control" name="period" 
									value="">
									<input id="showEasing" type="hidden" class="form-control" name="nik" 
									value="<?php echo $_SESSION['user_session_android'][0]->username; ?>">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary">Proses </button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>
