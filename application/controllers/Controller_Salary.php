	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Controller_Salary extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->library('session');
			$this->load->model('model_salary');
			$this->load->model('model_employee');
			$this->load->library('SimpleXLSX');
			cek_session();

		} 
		public function index(){
			$data['data'] = $this->model_employee->get_karyawan_by_nik($_SESSION['user_session_android'][0]->username);
			$this->load->view('template/Sidebar');
			$this->load->view('salary.php',$data);
			$this->load->view('template/Footer');
		}

		
		function print_preview(){
			$data['data'] = $this->model_salary->get_data_by_nik_and_period($_GET['nik'],$_GET['period']);
			$this->load->view('template/Sidebar');
			$this->load->view('print_preview.php',$data);
			$this->load->view('template/Footer');
		}
		
		function print_slip_gaji(){
			$data['data'] = $this->model_salary->get_data_by_nik_and_period($_GET['nik'],$_GET['period']);
			if ($data['data'] != null) {
				$this->load->helper('dompdf');
				$html = $this->load->view('print_salary_slip',$data,true);
				$filename = "Slip";
				$paper = 'A4';
				$orientation = 'landscape';
				pdf_create($html, $filename, $paper, $orientation, 'Print Slip Gaji');
			}
			$data['heading'] = 'Cetak Gaji';
			$data['message'] = 'Data tidak ditemukan';
			$this->load->view('errors/html/error_404.php',$data);
		}
	}