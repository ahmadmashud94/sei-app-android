	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Controller_Home extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->library('session');
			$this->load->model('models');
			cek_session();

		} 
		public function index(){
			$this->load->view('template/Sidebar');
			// $this->load->view('salary.php');
			$this->load->view('template/Footer');
		}

	}