<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;
use Dompdf\Options;
function pdf_create($html, $filename='', $paper, $orientation, $title, $stream=TRUE) 
{
    
    $options = new Options();
    $options->set('isRemoteEnabled',true);

// instantiate and use the dompdf class
    $contxt = stream_context_create([ 
        'ssl' => [ 
            'verify_peer' => FALSE, 
            'verify_peer_name' => FALSE,
            'allow_self_signed'=> TRUE
        ] 
    ]);
    $dompdf = new DOMPDF($options);
    $dompdf->setHttpContext($contxt);
    $dompdf->set_paper($paper,$orientation);
    $dompdf->load_html($html);
    $dompdf->render();
    $dompdf->add_info('Title', $title);

    if ($stream) {
        $dompdf->stream($filename.".pdf",array('Attachment'=>0));
    } else {
        return $dompdf->output();
    }
}
?>